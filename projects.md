---
layout: list_items_with_modals
title: "Projects"
---

In this page you can find all projects FLUSP actively contributes to.
We work in a very descentralized way, meaning each project and their
members are responsible for their own structure and organization.

Each project has a list of contributors and mentors. Contributors are
FLUSP members that actively add, remove and review code. Mentors are
the contributors with the most knowledge of the project. Every
contributor is expected to help newbies, but mentors are especially
suited for this, as they are the ones with the most experience. Mentors
are also responsible for reviewing code if a patch is left in wait for
review for too long. This position is temporary, and mentors should
always be ready to "hand over the baton" once a non-mentor contributor
has shown to be capable and willing to take these responsabilities.

Below you can find a list of projects we are currently actively
contributing to. If you wish to start contributing with any one (or
more) of these projects, send an email to
<span style="unicode-bidi: bidi-override; direction: rtl;">moc.spuorgelgoog@psulf</span>
(the email address is obfuscated) with the subject prepended with the
tag [PROJECT NAME] and let us know!
