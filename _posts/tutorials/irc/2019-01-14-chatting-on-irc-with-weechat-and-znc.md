---
layout: post
categories: others
title: "Chatting on IRC with Weechat and ZNC"
author: renatogeh
excerpt_separator: <!--end-abstract-->
---

This tutorial provides a simple guide for setting up your WeeChat
environment with ZNC for chatting and logging on IRC. We first cover
WeeChat installation. We then explain how to register your username with
NickServ on FreeNode and OFTC. Finally, we show how to configure your
WeeChat to work with ZNC.

<!--end-abstract-->

## Introduction

[Internet Relay
Chat](https://en.wikipedia.org/wiki/Internet_Relay_Chat), or simply IRC,
is a protocol for text communication amongst humans on the internet.
FLOSS communities often use IRC due to its simplicity, plain text
format, and just outright stubbornness from the programming community.
One may find official channels for many free software communities, such
as FreeBSD, Debian, Arch Linux and a bunch of channels for different
submodules in the Linux Kernel. There are many ways to chat on IRC.
Options include graphical applications such as
[HexChat](https://hexchat.github.io/), web clients
([KiwiIRC](https://kiwiirc.com)), or ncurses clients like
[irssi](https://irssi.org/) and [WeeChat](http://weechat.org).

Now, why use IRC when you can have any other "hip", "trending", chat
protocol that eats up your RAM with their Electron clone of Chromium or
data mines you, selling your private information to marketing companies?
Well, one could argue that they just want a simple text chat protocol
that [works in their Commodore64](https://youtu.be/0TTYcqeVO8k). Or
maybe they just want a place where they can find any kind of interesting
content, from a channel where you can only [say sentences that have
never been said
before](https://blog.xkcd.com/2008/01/14/robot9000-and-xkcd-signal-attacking-noise-in-chat/)
(`irc.foonetic.net`, `#xkcd-signal`), to one where you can only say
sentences that *have* been said before (`irc.foonetic.net`,
`#xkcd-noise`).  Regardless of their niche reasons, IRC is definitely
the place to get in touch with the free software community and almost
instantaneously get help and advice from grizzled veterans of the
programming community.

In this tutorial, we provide a guide on how to enter the wonderful world
of IRC through WeeChat, a highly customizable ncurses IRC client. We
then show how to register your username through NickServ. Finally, we
replace directly connecting to IRC servers with a ZNC bouncer.

## WeeChat

WeeChat is available in various shapes and forms. Remote interfaces for
WeeChat include a [web](https://www.glowing-bear.org/) version, an
[Android](https://github.com/ubergeek42/weechat-android) app, a [Qt
GUI](https://weechat.org/download/devel/) and even as an [Emacs
plugin](https://github.com/the-kenny/weechat.el) (*shivers*). However,
in this tutorial we will be covering only the ncurses, official version,
which should look somewhat like this:

{% include add_image.html src="weechat.png" caption="The WeeChat Client" %}

WeeChat should be available in most package managers. A list of packages
for Debian, Ubuntu and Raspbian are available on [their
site](https://weechat.org/download/debian/active/).

For Arch Linux, a community package is available as `weechat`.

Alternatively, you may download the
[source](https://weechat.org/download/stable/) and compile it yourself.

I won't cover usage of WeeChat on this tutorial. That is up to you. Here
are the [FAQ](https://weechat.org/files/doc/stable/weechat_faq.en.html),
[Quick Start Guide](https://weechat.org/files/doc/stable/weechat_quickstart.en.html)
and [User Guide](https://weechat.org/files/doc/stable/weechat_user.en.html).
Highly recommend reading the Quick Start Guide and configuring your
WeeChat now before proceeding.  The FAQ and User Guide are decent as
reference guides.

If you're using a Window Manager like [i3](https://i3wm.org) you may not
receive notifications when being "pinged" (i.e. someone calls you by
your username). This is because some window managers don't come with any
notification daemons installed by default. To fix this, you may either
install a notification daemon by some Desktop Environment (like GNOME)
or install [Dunst](https://dunst-project.org/). Dunst is a lightweight
notification daemon that fits well into minimalistic window managers
like i3.

In Arch Linux, Dunst can be easily installed through `pacman`.

```
pacman -S dunst
```

Setting up a notification agent on WeeChat can be done through plugins.
[Here](https://weechat.org/scripts/stable/tag/notify/) are some. Some
are text-only and others are able to play a sound when pinged.

You can install a plugin via the `/script` command. As an example, we'll
install the `autosort.py` script, that groups channels with same server
together and sorts them alphabetically (though the sorting criteria is
customizable), and which I highly recommend having in your WeeChat
environment.

```
/script install autosort.py
/script autoload autosort.py
```

This'll install and start the `autosort.py` plugin for every WeeChat
session. In the case of `autosort.py`, we'll also have to unmerge how
WeeChat organizes buffers.

```
/set irc.look.server_buffer independent
```

Once you've set everything to your taste, let us add FreeNode and OFTC
as servers.

```
/server add freenode irc.freenode.net
/server add oftc irc.oftc.net
/save
```

And connect to FreeNode.

```
/connect freenode
```

Now we shall begin registering your username on...

## NickServ

NickServ is an IRC bot with the goal of registering and managing users
and their nicknames. Its channel equivalent ChanServ does the same, but
for (duh) channels. You can read more about nickname registration
[here](https://freenode.net/kb/answer/registration).

IRC is a free-for-all, first-come-first-served anarchic mayhem. That
means anyone can have any username if it is not registered with
NickServ. That's why we highly recommend registering yours. Here's how:

Go to the `freenode` buffer on WeeChat. Once there, register your
username with NickServ.

First enter your desired nickname through the `/nick` command.

```
/nick nickname
```

Then, send a private message to NickServ asking to register your
current nickname to your email.

```
/msg NickServ REGISTER password name@email.com
```

Read carefully what NickServ has to say and follow the instructions.
Once you've done that, do the same with OFTC. Note that NickServ on OFTC
has a slightly different syntax. For `REGISTER` though, they are both
identical.

In the case of OFTC's read extra carefully what they have to say once
you've entered your `REGISTER` request. You'll have to go to their
website and manually verify your account.

Done! You're registered with NickServ. Now, let's get you on...

## ZNC

[ZNC](https://znc.in) is a [free,
open-source](https://github.com/znc/znc) IRC bouncer. What that means is
that, instead of connecting to the servers (e.g. FreeNode or OFTC)
directly, you connect to a ZNC server. The ZNC server, in turn, connects
you to the IRC servers.

Now, why would we want that? Well, for starters, if you have used IRC
before, you may have noticed that once you shutdown your client, you are
no longer connected to the servers. That means you get no messages from
other users when you're offline, nor can others reach you. And if you
haven't registered your username on NickServ, your username is now free
for the taking!

So what a bouncer does is keep you always online. ZNC, for its part,
allows multiple users sharing the same server, provides a simple web UI,
allows configuring multiple networks and channels, and also has a
variety of modules that make an IRCer life much more comfortable.

FLUSP runs a ZNC bouncer on its server. You can access it through its
[web UI](https://flusp.ime.usp.br/znc/).

{% include add_image.html src="znc.png" caption="ZNC Web UI" %}

Now ask any [FLUSP-Infra](https://gitlab.com/flusp/flusp-infra)
contributor to create your account.

Once your account has been created, log in and change your password
through your settings page.  Press `Save and continue` on the bottom of
the page. We are now going to configure your ZNC account.

The **IRC Information** section contains basic IRC information. Change
your *Nickname* to the one you set during the NickServ section.

{% include add_image.html src="znc_1.png" caption="IRC Information" %}

- *Alt. Nickname* is the nickname the IRC server is going to assign to you
if *Nickname* is already taken;
- *Ident* and *Realname* are ID names that nobody uses, so you can leave
them as is;
- *Quit Message* is what is going to be printed when you leave a channel.

Head over to the next section **Networks** and press *Add*. We're now
going to add the FreeNode network.

{% include add_image.html src="znc_2.png" caption="FreeNode Settings" %}

Set *Network Name* as `freenode` and *Nickname* as your FreeNode
NickServ chosen nickname. On *Servers of this IRC network*, add
`chat.freenode.net 6667`. The *Channels* section allows you to add
channels to your ZNC configuration. Click *Add* and add the `#ccsl-usp`
channel to the network. I recommend a 500 buffer count for this
particular channel.  Press *Add Channel and return*.

{% include add_image.html src="znc_3.png" caption="FreeNode Channels" %}

Head over to the *Modules* section and tick *nickserv*. On the text
field next to it, insert your NickServ password you set earlier. Once
done that, press *Add network and return* on the bottom of the page.

{% include add_image.html src="znc_4.png" caption="`nickserv` Module" %}

Your *Network* section should look something like this:

{% include add_image.html src="znc_5.png" caption="Network Section" %}

Click *Add* to add the OFTC network. Do the same as FreeNode, but with
*Network Name* as `oftc` and *Servers of this IRC network* as

```
irc.oftc.net 6667
irc.oftc.net 6668
irc.oftc.net 6670
irc.oftc.net 7000
```

instead. Add the `#kernelnewbies` channel and do the same with the
`nickserv` module as we did before with FreeNode. Save and return.

That's it! Well, at least from the ZNC UI's side. Head back to WeeChat
and let's add the ZNC server networks.

## Integrating WeeChat with ZNC

We are now going to add each ZNC network at a time. Let's first add
FreeNode.

```
/server add znc_freenode flusp.ime.usp.br/1212 -ssl -username=username/freenode -password=password -autoconnect
```

Where username and password are your username and password set with
NickServ.

```
/connect znc_freenode
/save
```

You should now be connected with FreeNode. Now let's do the same for OFTC.

```
/server add znc_oftc flusp.ime.usp.br/1212 -ssl -username=username/oftc -password=password -autoconnect
/connect znc_oftc
/save
```

And now you're connected to OFTC!

And there you go! You are connected to ZNC and able to chat through
WeeChat. Feel free to add networks and channels to your ZNC account if
you want to. If ZNC complains about having a network limit, ask a
FLUSP-Infra contributor to raise yours.

Have fun IRCing! See you on `#ccsl-usp`!
